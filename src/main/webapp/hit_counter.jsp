<%@ page import="java.util.concurrent.atomic.AtomicInteger" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/basic.css">
    <title>Hit Counter</title>
</head>
<%
    System.out.println("[debug] start of hit_counter code");
    String hitCounterAttributeName = "hit_counter_attribute";
    AtomicInteger hitCounter = (application.getAttribute(hitCounterAttributeName) == null) ? new AtomicInteger() : (AtomicInteger) application.getAttribute(hitCounterAttributeName);
    application.setAttribute(hitCounterAttributeName, new AtomicInteger(hitCounter.incrementAndGet()));
    System.out.println("[debug] end of hit_counter code");
%>
<body>
<h1 class="centered">
    Hit Counter
</h1>
<h2 class="centered">
    Number of Visits : <%=hitCounter.get()%>
</h2>
<a href="/" class="centered">Home</a>
</body>
</html>
