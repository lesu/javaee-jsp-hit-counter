<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/basic.css">
    <title>Home</title>
</head>
<body>
<h1 class="centered">
    Home
</h1>
<ul class="centered">
    <li>
        <a href="hit_counter.jsp">Hit Counter</a>
    </li>
    <li>
        <a href="test.jsp">Test</a>
    </li>
</ul>
</body>
</html>
